package com.faisalk.shortcutselenium.webelements;

import org.openqa.selenium.By;

public interface BlueprintofFramework {
	ElementAction findElement(By locator);

	ElementAction selectElement(String text);

	ElementAction selectElement(int indexValue);

	ElementAction deSelectAll();

	ElementAction deselectText(String text);

	ElementAction highlightElement();

	ElementAction unHighlighElement();

	ElementAction enterText(String text);

	public void submit();

	public void pressEnter();
}
