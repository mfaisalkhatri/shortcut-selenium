package com.faisalk.shortcutselenium.webelements;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class ElementAction implements BlueprintofFramework {

	WebElement element;
	WebDriver driver;

	public ElementAction(WebDriver driver) {
		this.driver = driver;
	}

	public ElementAction findElement(By locator) {
		element = driver.findElement(locator);
		return this;
	}

	public ElementAction selectElement(String text) {
		Select select = new Select(element);
		select.selectByValue(text);
		return this;
	}

	public ElementAction selectElement(int indexValue) {
		Select select = new Select(element);
		select.selectByIndex(indexValue);
		return this;

	}

	public ElementAction highlightElement() {
		JavascriptExecutor js = (JavascriptExecutor) driver;

		js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);

		return this;
	}

	public ElementAction unHighlighElement() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].setAttribute('style',arguments[1]);", element);
		return this;
	}

	public void submit() {
		element.submit();


	}

	public void pressEnter() {
		element.sendKeys(Keys.ENTER);
	}

	public ElementAction deSelectAll() {
		Select select = new Select(element);
		select.deselectAll();
		return this;
	}

	@Override
	public ElementAction deselectText(String text) {
		Select select = new Select(element);
		select.deselectByVisibleText(text);
		return this;

	}

	public ElementAction enterText(String text) {
		element.sendKeys(text);
		return this;
	}

}
