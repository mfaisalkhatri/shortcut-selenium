package com.faisalk.shortcutselenium.setbrowser;

import org.openqa.selenium.WebDriver;

public interface BrowserSetup {

	public WebDriver setbrowser();

	public void maximizeWindow();

	public void openSite(String url);

	public void getCurrentTitle();

	public void closeBrowser();

	public void quitBrowser();

}
