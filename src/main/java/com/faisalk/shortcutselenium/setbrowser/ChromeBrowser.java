package com.faisalk.shortcutselenium.setbrowser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ChromeBrowser implements BrowserSetup {

	protected WebDriver driver;

	public WebDriver setbrowser() {

		final String exe = "chromedriver.exe";
		final String path = getClass().getClassLoader().getResource(exe).getPath();

		System.setProperty("webdriver.chrome.driver", path);

		driver = new ChromeDriver();
		System.out.println("Chrome Launched");
		maximizeWindow();
		return driver;

	}

	public void maximizeWindow() {
		driver.manage().window().maximize();

	}

	public void openSite(String url) {

		driver.get(url);

	}

	public void getCurrentTitle() {

		String title = driver.getTitle();
		System.out.println("Title is " + title);
	}

	public void closeBrowser() {
		if (driver != null) {
			driver.close();
			System.out.println("Browser Closed");

		}

	}

	public void quitBrowser() {

		if (driver != null) {
			driver.quit();
			System.out.println("Browser Quit");
		}

	}

}
