package com.faisalk.shortcutselenium.setbrowser;

public class BrowserFactory {

	public BrowserSetup getBrowser(String browserType) {

		if (browserType == null) {
			System.out.println("Error in Config File");
			return null;
		}

		if (browserType.equalsIgnoreCase("CHROME")) {
			return new ChromeBrowser();
		}

		else if (browserType.equalsIgnoreCase("FIREFOX")) {
			return new FirefoxBrowser();
		}

		return null;

	}
}
