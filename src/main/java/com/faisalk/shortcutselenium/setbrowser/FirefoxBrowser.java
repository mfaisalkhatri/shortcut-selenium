package com.faisalk.shortcutselenium.setbrowser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxBrowser implements BrowserSetup {

	private WebDriver driver;

	public WebDriver setbrowser() {

		final String exe = "geckodriver.exe";
		final String path = getClass().getClassLoader().getResource(exe).getPath();

		System.setProperty("webdriver.gecko.driver", path);

		driver = new FirefoxDriver();
		System.out.println("Firefox Launched");
		maximizeWindow();

		return driver;
	}

	public void openSite(String url) {
		driver.get(url);

	}

	public void maximizeWindow() {

		driver.manage().window().maximize();

	}

	public void getCurrentTitle() {

		String title = driver.getTitle();
		System.out.println("Title is " + title);
	}

	public void closeBrowser() {
		if (driver != null) {
			driver.close();
			System.out.println("Browser Closed");
		}
	}

	public void quitBrowser() {

		if (driver != null) {
			driver.quit();
			System.out.println("Browser Quit");
		}

	}

}
