package com.faisalk.shortcutselenium.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.faisalk.shortcutselenium.config.PropertiesReader;
import com.faisalk.shortcutselenium.setbrowser.BrowserFactory;
import com.faisalk.shortcutselenium.setbrowser.BrowserSetup;
import com.faisalk.shortcutselenium.webelements.ElementAction;

public class BrowserTest {

	@Test
	public void test1() throws InterruptedException {

		WebDriver driver;
		BrowserFactory browser = new BrowserFactory();
		PropertiesReader prop = new PropertiesReader();
		String browserType = prop.getKey("browser");
		System.out.println(browserType);

		String url = prop.getKey("url");
		BrowserSetup brwser = browser.getBrowser(browserType);
		driver = brwser.setbrowser();
		brwser.openSite(url);
		brwser.getCurrentTitle();

		ElementAction elementact = new ElementAction(driver);
		ElementAction search = elementact.findElement(By.id("lst-ib"));
		search.highlightElement().enterText("What is Github").unHighlighElement().submit();

		brwser.closeBrowser();
	}

}
